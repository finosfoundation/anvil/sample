package pci_dss_validation

# Ensure that the ECS task definition does not allow privileged mode
violation[{"msg": msg}] {
    task_definition := input["module.example-app.aws_ecs_task_definition.web_task_definition"]

    task_definition.container_definitions[_].docker_security_options[_] == "seccomp=unconfined"
    msg := "The ECS task definition allows privileged mode"
}

# Ensure that the ECS task definition uses Fargate launch type
violation[{"msg": msg}] {
    task_definition := input["module.example-app.aws_ecs_task_definition.web_task_definition"]

    not contains(task_definition.requires_compatibilities, "FARGATE")
    msg := "The ECS task definition does not use Fargate launch type"
}

# Ensure that the ECS service is configured with the desired count of tasks
violation[{"msg": msg}] {
    service := input["module.example-app.aws_ecs_service.web_service"]
    desired_count := input["var.desired_count"]

    service.desired_count != desired_count
    msg := sprintf("The ECS service desired count (%d) does not match the expected value (%d)", [service.desired_count, desired_count])
}

# Ensure that the load balancer target group has a health check path
violation[{"msg": msg}] {
    target_group := input["module.example-app.aws_lb_target_group.web_target_group"]

    target_group.health_check.path == ""
    msg := "The load balancer target group does not have a health check path"
}

# Ensure that the load balancer listener rule has a path pattern condition
violation[{"msg": msg}] {
    listener_rule := input["module.example-app.aws_lb_listener_rule.web_listener_rule"]

    not listener_rule.condition.path_pattern
    msg := "The load balancer listener rule does not have a path pattern condition"
}

# Ensure that the security group allows traffic only from approved sources
violation[{"msg": msg}] {
    web_security_group := input["module.example-app.aws_security_group.web_security_group"]
    allowed_cidr_blocks := input["var.allowed_cidr_blocks"]

    not all_allowed(ip) {
        ip := allowed_cidr_blocks[_]
        contains(web_security_group.ip_permissions_flat[*].cidr_blocks, ip) with input
    }
    msg := "The security group allows traffic from unapproved sources"
}
