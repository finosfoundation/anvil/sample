FROM registry.access.redhat.com/ubi8/ubi:8.4

# Install required packages
RUN yum -y update && \
    yum -y install epel-release && \
    yum -y install git curl jq unzip && \
    yum clean all

# Install OPA
RUN curl -L -o opa https://openpolicyagent.org/downloads/latest/opa_linux_amd64 && \
    chmod +x opa && \
    mv opa /usr/local/bin/

# Install Gherkin verifier
RUN curl -L -o gherkin-verifier https://github.com/ArachneTech/gherkin-verifier/releases/download/v0.0.3/gherkin-verifier-linux-amd64 && \
    chmod +x gherkin-verifier && \
    mv gherkin-verifier /usr/local/bin/

# Install Terraform
RUN curl -L -o terraform.zip https://releases.hashicorp.com/terraform/1.0.5/terraform_1.0.5_linux_amd64.zip && \
    unzip terraform.zip && \
    chmod +x terraform && \
    mv terraform /usr/local/bin/ && \
    rm terraform.zip

# Add the OPA policy, Terraform configuration, and Gherkin script to the container
COPY policy.rego /app/
COPY terraform/ /app/terraform/
COPY test.feature /app/

WORKDIR /app/

# Verify the policy, Terraform configuration, and Gherkin script
RUN opa test -v policy.rego && \
    cd terraform && \
    terraform init && \
    terraform validate && \
    cd .. && \
    gherkin-verifier test.feature

# Set the entrypoint to run the OPA policy on input JSON data
ENTRYPOINT ["opa", "eval", "-i", "/dev/stdin", "policy.rego"]
