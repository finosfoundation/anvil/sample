resource "aws_ecs_task_definition" "web_task_definition" {
  family                   = "web-task"
  container_definitions    = jsonencode([{
    name = "web"
    image = var.web_image
    portMappings = [{
      containerPort = var.web_port
      hostPort = var.web_port
      protocol = "tcp"
    }]
  }])
  requires_compatibilities = ["FARGATE"]
  memory                   = 512
  cpu                      = 256
}

resource "aws_ecs_service" "web_service" {
  name            = "web-service"
  cluster         = var.cluster_name
  task_definition = aws_ecs_task_definition.web_task_definition.arn
  desired_count   = var.desired_count
  launch_type     = "FARGATE"

  network_configuration {
    assign_public_ip = true
  }
}

resource "aws_lb_target_group" "web_target_group" {
  name        = "web-target-group"
  port        = var.web_port
  protocol    = "HTTP"
  vpc_id      = var.vpc_id

  health_check {
    path = "/"
  }
}

resource "aws_lb_listener" "web_listener" {
  load_balancer_arn = var.load_balancer_arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.web_target_group.arn
  }
}

resource "aws_lb_listener_rule" "web_listener_rule" {
  listener_arn = aws_lb_listener.web_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.web_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/app/*"]
    }
  }
}

resource "aws_security_group_rule" "allow_http" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = var.allowed_cidr_blocks
  security_group_id = aws_security_group.web_security_group.id
}

resource "aws_security_group_rule" "allow_https" {
  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = var.allowed_cidr_blocks
  security_group_id = aws_security_group.web_security_group.id
}

resource "aws_security_group" "web_security_group" {
  name_prefix = "web-security-group-"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_groups = [aws_security_group.lb_security_group.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "lb_security_group" {
  name_prefix = "lb-security-group-"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
  }
}
